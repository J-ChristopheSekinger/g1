Merci à 1000i100 pour son travail mis à disposition sous licence CC-0 :
![](geconomicus.png)

Version SVG éditable :
![](geconomicus.svg)

Police de caractère utilisée : [Agreloy](https://www.1001fonts.com/agreloy-font.html) sous licence  SIL Open Font License (OFL) réalisé par GLUK


-----

Quel meilleur moyen pour comprendre les tenants et les aboutissants d'un système monétaire que de l'expérimenter par le jeu ? C’est ce que nous propose le Ğeconomicus, un “jeu de société” qui simule un système économique sur 80 ans d’existence.

Concrétement, nous expérimenterons trois types de création monétaire différents :
- celui du troc, sans monnaie
- celui de l’euro, appelé monnaie dette (car la création monétaire s'y fait par contraction de dettes auprès d’une banque)
- celui d’une [monnaie libre](https://monnaie-libre.fr/) dont la création monétaire se fait par dividende universel.

S'en suivra un temps de bilan pour comparer les impacts émotionels et économiques de ces trois fonctionnements. Cela au travers de vos impressions et des scores de chaque "monnaie", puis nous répondrons aux questions.


Une minute de vidéo pour mieux comprendre le Ǧéconomicus :

<iframe title="Découvrez le jeu Ğeconomicus, simulateur de systèmes monétaires - MFRB" src="https://tube.aquilenet.fr/videos/embed/9170115a-f706-43a9-bf7a-4cf08559b18a?stop=59s&amp;start=7s&amp;warningTitle=0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="800" height="450" frameborder="0"></iframe>

Nous déconseillons fortement de partir avant d'avoir expérimenté chaque mode de jeu - votre vécu en serait dénaturé - vous pouvez en revanche vouss retirer d'une phase pour reprendre à la suivante. Prévoyez 3 à 4h de jeu + bilan ou prévenez-nous d'avance si vous ne pouvez rester jusqu'au bout.

